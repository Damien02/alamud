function start_websocket() {
    $(document).ready(function() {
	if (!window.console) window.console = {};
	if (!window.console.log) window.console.log = function() {};

	$("#messageform").on("submit", function() {
            newMessage($(this));
            return false;
	});
	$("#messageform").on("keypress", function(e) {
            if (e.keyCode == 13) {
		newMessage($(this));
		return false;
            }
	});
	$("#command").select();
	updater.start();
    });
}

function newMessage(form, msg=null) {
    if(msg == null){
		var message = form.formToDict();
		updater.socket.send(JSON.stringify(message));
		form.find("input[type=text]").val("").select();
	}else{
		var message = msg;
		updater.socket.send(JSON.stringify(message));
	}
}

jQuery.fn.formToDict = function() {
    var fields = this.serializeArray();
    var json = {}
    for (var i = 0; i < fields.length; i++) {
        json[fields[i].name] = fields[i].value;
    }
    if (json.next) delete json.next;
    return json;
};

function saveMessage() {
    var message = {"type":"save"};
    updater.socket.send(JSON.stringify(message));
    form.find("input[type=text]").val("").select();
}

function resetMessage() {
    var message = {"type":"reset"};
    updater.socket.send(JSON.stringify(message));
    form.find("input[type=text]").val("").select();
}

function scroll_to_bottom() {
    $("#inbox").animate({scrollTop:$("#inboxcontents").height()}, 0);
    //$("#inbox").animate({scrollTop:$("#inboxcontents").height()}, 1000);
}

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/websocket";
	updater.socket = new WebSocket(url);
	updater.socket.onmessage = function(event) {
	    updater.showMessage(JSON.parse(event.data));
	}
    },

    showMessage: function(message) {
	var html = "<div class=\"alert mud-"+message.type+"\">"+message.html+"</div>"
        $("#inboxcontents").append(html);
	scroll_to_bottom();
    }
};

var liste = [
	"prendre lampe","haut","prendre badge","bas",
	"ouest","prendre flambeau",
	"bas","ne","ouvrir porte avec badge","ne",
	"allumer lampe","est","prendre briquet"
]
/*
	"allumer flambeau avec briquet", "bas"
]*/

function auto(commandes){
	for(var i = 0; i < commandes.length; i++){
		newMessage(null,{ text: commandes[i], type: "input", _xsrf: "02c6ee348dfc4028887cc8afc24511c4" })
	}
}
