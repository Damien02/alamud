from .event import Event3

class BreakIntoEvent(Event3):
	NAME = "break-into"
	
	def perform(self):
		self.inform("break-into")
